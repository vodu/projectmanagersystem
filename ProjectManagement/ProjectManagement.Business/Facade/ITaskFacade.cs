﻿using ProjectManagement.Business.Dto;

namespace ProjectManagement.Business.Facade
{
	public interface ITaskFacade : ICrudFacade<TaskPostDto, TaskGetDto>
	{
	}
}
