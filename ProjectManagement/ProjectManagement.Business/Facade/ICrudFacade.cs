﻿using ProjectManagement.Business.Dto;
using System.Collections.Generic;

namespace ProjectManagement.Business.Facade
{
	public interface ICrudFacade<TPostDto, TGetDto> where TPostDto : BasePostDto where TGetDto : BaseGetDto
	{
		TGetDto Create(TPostDto source);
		TGetDto Get(int id);
		TGetDto Update(TPostDto source);
		bool? Delete(int id);
		IEnumerable<TGetDto> GetByCriteria(SearchCriteriaDto searchCriteria);
	}
}
