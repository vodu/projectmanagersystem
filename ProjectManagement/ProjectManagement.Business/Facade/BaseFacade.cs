﻿using Microsoft.EntityFrameworkCore;
using ProjectManagement.Data;
using ProjectManagement.Data.Entities;
using System;
using System.Linq;

namespace ProjectManagement.Business.Facade
{
	public class BaseFacade : IDisposable
	{
		protected readonly ProjectManagementContext context;
		public BaseFacade(ProjectManagementContext context)
		{
			this.context = context;
		}

		protected HierarchyNode CreateNode(int? parentId)
		{
			var node = new HierarchyNode { ParentId = parentId };
			var newLeft = 1;
			if (parentId.HasValue)
			{
				var parentNode = context.Hierarchy.FirstOrDefault(h => h.Id == parentId);
				if (parentNode == null)
					return null;

				newLeft = parentNode.Right;

				var rightUpdate = context.Hierarchy.Where(h => h.Right >= newLeft);
				foreach (var item in rightUpdate)
				{
					item.Right += 2;
				}

				var leftUpdate = context.Hierarchy.Where(h => h.Left > newLeft);
				foreach (var item in leftUpdate)
				{
					item.Left += 2;
				}
			}
			else
			{
				if (context.Hierarchy.Any())
					newLeft = context.Hierarchy.Max(h => h.Right) + 1;
			}

			node.Left = newLeft;
			node.Right = newLeft + 1;

			return node;
		}

		protected void DeleteNode(HierarchyNode node)
		{
			var width = node.Right - node.Left + 1;

			var downNodes = context.Hierarchy.Where(h => h.Left >= node.Left && h.Left <= node.Right);
			context.RemoveRange(downNodes);
			var rightUpdate = context.Hierarchy.Where(h => h.Right > node.Right);
			foreach (var item in rightUpdate)
			{
				item.Right -= width;
			}

			var leftUpdate = context.Hierarchy.Where(h => h.Left > node.Right);
			foreach (var item in leftUpdate)
			{
				item.Left -= width;
			}

			context.SaveChanges();
		}

		protected void UpdateUpProjects(HierarchyNode node)
		{
			var upProjects = context.Projects.Include(p => p.Node).Where(p => p.Node.Left <= node.Left && p.Node.Right >= node.Right).ToList();

			upProjects.ForEach(project =>
			{
				var downTasks = context.Tasks.Include(p => p.Node)
					.Where(t => t.Node.Left >= project.Node.Left && t.Node.Left <= project.Node.Right && t.State != ProjectTaskState.Completed)
					.Select(t => t.State).ToList();
				if (!downTasks.Any())
					project.State = ProjectTaskState.Completed;
				else if (downTasks.Any(t => t == ProjectTaskState.InProgress))
					project.State = ProjectTaskState.InProgress;
				else
					project.State = ProjectTaskState.Planned;
			});
		}

		#region IDisposable

		bool disposed = false;

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
		protected virtual void Dispose(bool disposing)
		{
			if (disposed)
				return;

			if (disposing)
				context.Dispose();

			disposed = true;
		}

		~BaseFacade()
		{
			Dispose(false);
		}

		#endregion
	}
}
