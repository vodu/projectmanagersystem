﻿using ProjectManagement.Business.Dto;
using System.Collections.Generic;

namespace ProjectManagement.Business.Facade
{
	public class ExportImportFacade : IExportImportFacade
	{
		private readonly IProjectFacade _projectFacade;
		private readonly ITaskFacade _taskFacade;

		public ExportImportFacade(IProjectFacade projectFacade, ITaskFacade taskFacade)
		{
			_projectFacade = projectFacade;
			_taskFacade = taskFacade;
		}

		public IEnumerable<BaseGetDto> Export(SearchCriteriaDto searchCriteria)
		{
			var list = new List<BaseGetDto>();

			list.AddRange(_projectFacade.GetByCriteria(searchCriteria));
			list.AddRange(_taskFacade.GetByCriteria(searchCriteria));

			return list;
		}
	}
}
