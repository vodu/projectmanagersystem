﻿using ProjectManagement.Business.Dto;
using System.Collections.Generic;

namespace ProjectManagement.Business.Facade
{
	public interface IExportImportFacade
	{
		IEnumerable<BaseGetDto> Export(SearchCriteriaDto searchCriteria);
	}
}