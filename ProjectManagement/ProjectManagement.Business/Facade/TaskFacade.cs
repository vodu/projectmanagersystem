﻿using Microsoft.EntityFrameworkCore;
using ProjectManagement.Business.Dto;
using ProjectManagement.Data;
using ProjectManagement.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectManagement.Business.Facade
{
	public class TaskFacade : BaseFacade, ITaskFacade
	{
		public TaskFacade(ProjectManagementContext context) : base(context) { }
		public TaskGetDto Create(TaskPostDto source)
		{
			using (var transaction = context.Database.BeginTransaction())
			{
				var node = CreateNode(source.ParentId);
				if (node == null)
					return null;

				var entity = new Task
				{
					Node = node,
					Description = source.Description,
					Name = source.Name,
					StartDate = source.StartDate,
					FinishDate = source.FinishDate,
					State = ProjectTaskState.Planned
				};

				context.Tasks.Add(entity);
				context.SaveChanges();

				SetPlannedProjectState(node);

				transaction.Commit();

				return EntityToDto(entity);
			}
		}

		public TaskGetDto Get(int id)
		{
			return EntityToDto(context.Tasks.Include(t => t.Node).FirstOrDefault(t => t.Node.Id == id));
		}

		public TaskGetDto Update(TaskPostDto source)
		{
			using (var transaction = context.Database.BeginTransaction())
			{
				var entity = context.Tasks.Include(t => t.Node).FirstOrDefault(t => t.Node.Id == source.Id);
				if (entity == null)
					return null;

				var newState = Enum.Parse<ProjectTaskState>(source.State);
				var stateChanged = entity.State != newState;

				entity.Description = source.Description;
				entity.Name = source.Name;
				entity.StartDate = source.StartDate;
				entity.FinishDate = source.FinishDate;
				entity.State = newState;

				context.SaveChanges();

				if (stateChanged)
				{
					if (entity.State == ProjectTaskState.Planned)
						SetPlannedProjectState(entity.Node);
					else
						UpdateUpProjects(entity.Node);
				}


				transaction.Commit();
				return EntityToDto(entity);
			}
		}

		public bool? Delete(int id)
		{
			using (var transaction = context.Database.BeginTransaction())
			{
				try
				{
					var task = context.Tasks.Include(t => t.Node).FirstOrDefault(t => t.Node.Id == id);
					var node = task.Node;
					if (node == null)
						return null;

					var downTasks = context.Tasks.Include(t => t.Node).Where(t => t.Node.Left >= node.Left && t.Node.Left <= node.Right);

					context.RemoveRange(downTasks);
					context.SaveChanges();

					UpdateUpProjects(node);

					context.SaveChanges();

					DeleteNode(node);

					transaction.Commit();
				}
				catch
				{
					return false;
				}
			}

			return true;
		}

		public IEnumerable<TaskGetDto> GetByCriteria(SearchCriteriaDto searchCriteria)
		{
			var query = context.Tasks.Include(p => p.Node).AsQueryable();
			if (!string.IsNullOrWhiteSpace(searchCriteria.State))
			{
				if (Enum.TryParse<ProjectTaskState>(searchCriteria.State, out var state))
					query = query.Where(p => p.State == state);
			}

			if (searchCriteria.FinishDate.HasValue)
			{
				query = query.Where(p => p.FinishDate == searchCriteria.FinishDate.Value);
			}

			return query.ToList().Select(p => EntityToDto(p));
		}

		private TaskGetDto EntityToDto(Task source)
		{
			if (source == null)
				return null;

			return new TaskGetDto
			{
				Id = source.Node.Id,
				ParentId = source.Node.ParentId,
				Description = source.Description,
				Name = source.Name,
				StartDate = source.StartDate,
				FinishDate = source.FinishDate,
				State = source.State.ToString()
			};
		}

		private void SetPlannedProjectState(HierarchyNode node)
		{
			var upProjects = context.Projects.Include(p => p.Node).Where(p => p.Node.Left <= node.Left && p.Node.Right >= node.Right).ToList();
			upProjects.ForEach(project => project.State = ProjectTaskState.Planned);
			context.SaveChanges();
		}
	}
}
