﻿using Microsoft.EntityFrameworkCore;
using ProjectManagement.Business.Dto;
using ProjectManagement.Data;
using ProjectManagement.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectManagement.Business.Facade
{
	public class ProjectFacade : BaseFacade, IProjectFacade
	{
		public ProjectFacade(ProjectManagementContext context) : base(context) { }

		public IEnumerable<ProjectGetDto> ListProjects()
		{
			return context.Projects.Include(p => p.Node).AsEnumerable().Select(source => EntityToDto(source));
		}

		public ProjectGetDto Create(ProjectPostDto source)
		{
			using (var transaction = context.Database.BeginTransaction())
			{
				var node = CreateNode(source.ParentId);
				if (node == null)
					return null;

				var entity = new Project
				{
					Node = node,
					Code = source.Code,
					Name = source.Name,
					StartDate = source.StartDate,
					FinishDate = source.FinishDate,
					State = ProjectTaskState.Completed
				};

				context.Hierarchy.Add(node);
				context.Projects.Add(entity);
				context.SaveChanges();

				transaction.Commit();

				return EntityToDto(entity);
			}
		}

		public ProjectGetDto Get(int id)
		{
			return EntityToDto(context.Projects.Include(p => p.Node).FirstOrDefault(p => p.Node.Id == id));
		}

		public ProjectGetDto Update(ProjectPostDto source)
		{
			using (var transaction = context.Database.BeginTransaction())
			{
				var entity = context.Projects.Include(p => p.Node).FirstOrDefault(p => p.Node.Id == source.Id);
				if (entity == null)
					return null;

				entity.Code = source.Code;
				entity.Name = source.Name;
				entity.StartDate = source.StartDate;
				entity.FinishDate = source.FinishDate;

				context.SaveChanges();

				transaction.Commit();
				return EntityToDto(entity);
			}
		}

		public bool? Delete(int id)
		{
			using (var transaction = context.Database.BeginTransaction())
			{
				try
				{
					var project = context.Projects.Include(p => p.Node).FirstOrDefault(p => p.Node.Id == id);
					var node = project.Node;
					if (node == null)
						return null;

					var downProjects = context.Projects.Include(p => p.Node).Where(p => p.Node.Left >= node.Left && p.Node.Left <= node.Right);
					var downTasks = context.Tasks.Include(t => t.Node).Where(t => t.Node.Left >= node.Left && t.Node.Left <= node.Right);

					context.RemoveRange(downProjects);
					context.RemoveRange(downTasks);
					context.SaveChanges();

					if (project.State != ProjectTaskState.Completed)
						UpdateUpProjects(node);

					context.SaveChanges();

					DeleteNode(node);

					transaction.Commit();
				}
				catch
				{
					return false;
				}
			}

			return true;
		}

		public IEnumerable<ProjectGetDto> GetByCriteria(SearchCriteriaDto searchCriteria)
		{
			var query = context.Projects.Include(p => p.Node).AsQueryable();
			if (!string.IsNullOrWhiteSpace(searchCriteria.State))
			{
				if (Enum.TryParse<ProjectTaskState>(searchCriteria.State, out var state))
					query = query.Where(p => p.State == state);
			}

			if (searchCriteria.FinishDate.HasValue)
			{
				query = query.Where(p => p.FinishDate == searchCriteria.FinishDate.Value);
			}

			return query.ToList().Select(p => EntityToDto(p));
		}


		private ProjectGetDto EntityToDto(Project source)
		{
			if (source == null)
				return null;

			return new ProjectGetDto
			{
				Id = source.Node.Id,
				ParentId = source.Node.ParentId,
				Code = source.Code,
				Name = source.Name,
				StartDate = source.StartDate,
				FinishDate = source.FinishDate,
				State = source.State.ToString()
			};
		}
	}
}
