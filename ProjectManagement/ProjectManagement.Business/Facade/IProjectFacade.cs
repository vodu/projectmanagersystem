﻿using ProjectManagement.Business.Dto;
using System.Collections.Generic;

namespace ProjectManagement.Business.Facade
{
	public interface IProjectFacade : ICrudFacade<ProjectPostDto, ProjectGetDto>
	{
		IEnumerable<ProjectGetDto> ListProjects();
	}
}
