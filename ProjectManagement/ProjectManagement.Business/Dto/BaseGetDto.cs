﻿using System;

namespace ProjectManagement.Business.Dto
{
	public class BaseGetDto
	{
		public int Id { get; set; }
		public int? ParentId { get; set; }
		public string Name { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime FinishDate { get; set; }
		public string State { get; set; }
	}
}
