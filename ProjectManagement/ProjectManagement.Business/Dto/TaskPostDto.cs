﻿namespace ProjectManagement.Business.Dto
{
	public class TaskPostDto : BasePostDto
	{
		public string Description { get; set; }
		public string State { get; set; }
	}
}
