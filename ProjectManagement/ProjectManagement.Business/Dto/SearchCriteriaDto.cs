﻿using System;

namespace ProjectManagement.Business.Dto
{
	public class SearchCriteriaDto
	{
		public string State { get; set; }
		public DateTime? FinishDate { get; set; }
	}
}