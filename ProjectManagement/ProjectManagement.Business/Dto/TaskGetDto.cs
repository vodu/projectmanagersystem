﻿namespace ProjectManagement.Business.Dto
{
	public class TaskGetDto : BaseGetDto
	{
		public string Description { get; set; }
	}
}
