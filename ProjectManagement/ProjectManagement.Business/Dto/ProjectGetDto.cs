﻿namespace ProjectManagement.Business.Dto
{
	public class ProjectGetDto : BaseGetDto
	{
		public string Code { get; set; }
	}
}
