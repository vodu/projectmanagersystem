﻿namespace ProjectManagement.Business.Dto
{
	public class ProjectPostDto : BasePostDto
	{
		public string Code { get; set; }
	}
}
