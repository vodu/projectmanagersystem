﻿using Microsoft.Extensions.DependencyInjection;
using ProjectManagement.Business.Facade;
using ProjectManagement.Data;

namespace ProjectManagement.Business
{
    public class CompositionRoot
    {
        public CompositionRoot() { }

        public static void InjectDependencies(IServiceCollection services)
        {
            services.AddScoped(typeof(ProjectManagementContext));
            services.AddScoped(typeof(IProjectFacade), typeof(ProjectFacade));
            services.AddScoped(typeof(ITaskFacade), typeof(TaskFacade));
            services.AddScoped(typeof(IExportImportFacade), typeof(ExportImportFacade));
        }
    }
}
