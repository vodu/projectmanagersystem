﻿using System.ComponentModel.DataAnnotations;

namespace ProjectManagement.Data.Entities
{
	public class HierarchyNode
	{
		[Key]
		public int Id { get; set; }
		public int? ParentId { get; set; }
		public int Left { get; set; }
		public int Right { get; set; }
	}
}
