﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProjectManagement.Data.Entities
{
    public class Project
    {
        [Key]
        public int Id { get; set; }
        public virtual HierarchyNode Node { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime FinishDate { get; set; }
        public ProjectTaskState State { get; set; }
    }
}
