﻿namespace ProjectManagement.Data.Entities
{
    public enum ProjectTaskState
    {
        Planned,
        InProgress,
        Completed
    }
}
