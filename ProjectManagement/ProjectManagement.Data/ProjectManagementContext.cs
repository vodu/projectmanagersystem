﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ProjectManagement.Data.Entities;

namespace ProjectManagement.Data
{
    public class ProjectManagementContext : DbContext
    {
        private readonly string _connectionString;
        public ProjectManagementContext(IConfiguration configuration)
        {
            _connectionString = configuration.GetSection("ConnectionStrings")["ProjectManagementConnection"];
        }

        public ProjectManagementContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public DbSet<HierarchyNode> Hierarchy { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Task> Tasks { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlServer(_connectionString);
    }
}
