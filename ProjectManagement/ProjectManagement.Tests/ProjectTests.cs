﻿using ProjectManagement.Business.Dto;
using ProjectManagement.Business.Facade;
using System;
using Xunit;

namespace ProjectManagement.Tests
{
	public class ProjectTest : BaseTest
	{
		[Fact]
		public void CreatingFailed()
		{
			var created = _projectFacade.Create(new ProjectPostDto { ParentId = 0 });
			Assert.Null(created);
		}

		[Fact]
		public void Creating()
		{
			var source = new ProjectPostDto
			{
				Name = "hello moto",
				Code = "csharp",
				StartDate = new DateTime(2020, 02, 01),
				FinishDate = new DateTime(2020, 02, 02)
			};

			var created = _projectFacade.Create(source);
			Assert.Equal(source.Name, created.Name);
			Assert.Equal(source.Code, created.Code);
			Assert.Equal(source.StartDate, created.StartDate);
			Assert.Equal(source.FinishDate, created.FinishDate);

			_projectFacade.Delete(created.Id);
		}

		[Fact]
		public void Updating()
		{
			var updateName = "first second";
			var created = _projectFacade.Create(new ProjectPostDto { Name = "second first" });
			var updated = _projectFacade.Update(new ProjectPostDto { Id = created.Id, Name = updateName });
			Assert.Equal(updateName, updated.Name);

			_projectFacade.Delete(created.Id);
		}

		[Fact]
		public void Deleting()
		{
			var created = _projectFacade.Create(new ProjectPostDto { Name = "just to delete" });
			var deleted = _projectFacade.Delete(created.Id);
			Assert.NotNull(deleted);
			Assert.True(deleted.Value);

			var removed = _projectFacade.Get(created.Id);
			Assert.Null(removed);
		}
	}
}
