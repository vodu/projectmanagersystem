﻿using ProjectManagement.Business.Dto;
using ProjectManagement.Business.Facade;
using ProjectManagement.Data;
using ProjectManagement.Data.Entities;
using Xunit;

namespace ProjectManagement.Tests
{
	public class TaskTest : BaseTest
	{
		[Fact]
		public void TaskCreation()
		{
			int y2021Id, aug2021Id;
			{
				var y2021 = _projectFacade.Create(new ProjectPostDto { Name = "2021" });
				y2021Id = y2021.Id;
				var aug2021 = _projectFacade.Create(new ProjectPostDto { ParentId = y2021.Id, Name = "2021.August" });
				aug2021Id = aug2021.Id;
				Assert.Equal(ProjectTaskState.Completed.ToString(), y2021.State);
				Assert.Equal(ProjectTaskState.Completed.ToString(), aug2021.State);

				var sea = _taskFacade.Create(new TaskPostDto { ParentId = aug2021.Id, Name = "go to the sea" });
				Assert.Equal(ProjectTaskState.Planned.ToString(), sea.State);
			}
			// instatiate second facade to refresh instances
			using (var facade = new ProjectFacade(new ProjectManagementContext(_options)))
			{
				var y2021 = facade.Get(y2021Id);
				Assert.Equal(ProjectTaskState.Planned.ToString(), y2021.State);
				var aug2021 = facade.Get(aug2021Id);
				Assert.Equal(ProjectTaskState.Planned.ToString(), aug2021.State);
			}

			using (var facade = new ProjectFacade(new ProjectManagementContext(_options)))
			{
				facade.Delete(aug2021Id);
				var y2021 = _projectFacade.Get(y2021Id);
				Assert.Equal(ProjectTaskState.Completed.ToString(), y2021.State);
				_projectFacade.Delete(y2021Id);
			}
		}
	}
}
