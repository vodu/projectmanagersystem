﻿using ProjectManagement.Business.Facade;
using ProjectManagement.Data;

namespace ProjectManagement.Tests
{
	public class BaseTest
	{
		protected readonly IProjectFacade _projectFacade;
		protected readonly ITaskFacade _taskFacade;
		//Transactions are not supported by the in-memory store.
		//protected readonly DbContextOptions _options = new DbContextOptionsBuilder<ProjectManagementContext>().UseInMemoryDatabase(databaseName: "ProjectManagement").Options;
		protected readonly string _options = "Server=.;Initial Catalog=ProjectManagement2;User=ProjectManagement;Password=ProjectManagement;MultipleActiveResultSets=True;";
		public BaseTest()
		{
			_projectFacade = new ProjectFacade(new ProjectManagementContext(_options));
			_taskFacade = new TaskFacade(new ProjectManagementContext(_options));
		}
	}
}
