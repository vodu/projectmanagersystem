﻿using Microsoft.AspNetCore.Mvc;
using ProjectManagement.Business.Dto;
using ProjectManagement.Business.Facade;
using System;
using System.Collections.Generic;

namespace ProjectManagement.Web.Controllers
{
	[ApiController]
	[Route("[controller]")]
	public class ExportImportController : ControllerBase
	{
		private readonly IExportImportFacade _facade;
		public ExportImportController(IExportImportFacade facade)
		{
			_facade = facade;
		}

		[HttpGet]
		[Route("completed")]
		public IEnumerable<BaseGetDto> Export(DateTime? date)
		{
			return _facade.Export(new SearchCriteriaDto { State = "Completed", FinishDate = date });
		}
	}
}
