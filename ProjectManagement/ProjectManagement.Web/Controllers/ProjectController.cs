﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectManagement.Business.Dto;
using ProjectManagement.Business.Facade;

namespace ProjectManagement.Web.Controllers
{
	public class ProjectController : CrudController<IProjectFacade, ProjectPostDto, ProjectGetDto>
	{
		public ProjectController(IProjectFacade facade) : base(facade)
		{
		}

		[HttpGet]
		[Route("list")]
		public IEnumerable<ProjectGetDto> Index()
		{
			return _facade.ListProjects();
		}
	}
}
