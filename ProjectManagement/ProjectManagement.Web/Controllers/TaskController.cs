﻿using ProjectManagement.Business.Dto;
using ProjectManagement.Business.Facade;

namespace ProjectManagement.Web.Controllers
{
	public class TaskController : CrudController<ITaskFacade, TaskPostDto, TaskGetDto>
	{
		public TaskController(ITaskFacade facade) : base(facade)
		{
		}
	}
}
