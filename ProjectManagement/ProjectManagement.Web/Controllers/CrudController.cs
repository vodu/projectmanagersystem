﻿using Microsoft.AspNetCore.Mvc;
using ProjectManagement.Business.Dto;
using ProjectManagement.Business.Facade;

namespace ProjectManagement.Web.Controllers
{
	[ApiController]
	[Route("[controller]")]
	public class CrudController<TFacade, TPost, TGet> : ControllerBase where TFacade : ICrudFacade<TPost, TGet> where TPost : BasePostDto where TGet : BaseGetDto
	{
		protected readonly TFacade _facade;

		public CrudController(TFacade facade)
		{
			_facade = facade;
		}

		[HttpPost]
		public IActionResult Create(TPost source)
		{
			var resource = _facade.Create(source);

			if (resource == null)
				return BadRequest($"Parent id {source.ParentId} doesn't exist.");

			return Ok(resource);
		}

		[HttpGet("{id}")]
		public IActionResult Read(int id)
		{
			var resource = _facade.Get(id);

			if (resource == null)
				return NotFound();

			return Ok(resource);
		}

		[HttpPut("{id}")]
		public IActionResult Update(TPost source)
		{
			var resource = _facade.Update(source);

			if (resource == null)
				return NotFound();

			return Ok(resource);
		}

		[HttpDelete("{id}")]
		public IActionResult Delete(int id)
		{
			var resource = _facade.Delete(id);

			if (resource == null)
				return NotFound();

			if (!resource.Value)
				return StatusCode(500);

			return Ok();
		}

	}
}